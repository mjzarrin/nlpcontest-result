# NLPContest Accuracy By Monte Carlo
import math
import numpy as np
import pandas as pd

# 
total_iterate = 10_000_000
round_iterate = 100_000

names = pd.Series(['Raesi', 'Rouhani', 'Mirsalim', 'Hashemitaba'])
nlpcontest_prediction = pd.Series([38.49, 57.78, 2.56, 1.10])
election_result = pd.Series([38.28, 57.14, 1.16, 0.52])
election_result_normalized = (100 * election_result / election_result.sum()).round(2)


nlpcontest_df = pd.DataFrame({'Candidates': names, 'NLPContest':nlpcontest_prediction, 'Election':election_result,  'Election Normalized':election_result_normalized})
max_error = math.inf


def calculate_mae_error(serie1, serie2):
    err = (serie1 - serie2).abs().mean()
    return err

def random_votes_maker():
    votes = np.random.random(4)
    votes = 100 * votes / votes.sum()
    votes = votes.round(2)
    return votes


def calculate_probability_by_monte_carlo_method(nlpcontest_mae,size):

    best_sample_votes = {'votes': [], 'err':max_error}
    best_round_votes = {'votes': [], 'err':max_error}
    j = 0
    for i in range(1, size):
        sample_votes = random_votes_maker()
        sample_err = calculate_mae_error(sample_votes, election_result_normalized)

        if sample_err <= nlpcontest_mae:
            print('That\'s A Hit on index ', i,' sample error: ',sample_err,'sample vote: ',sample_votes)
            j += 1

        if sample_err < best_round_votes['err']:
            best_round_votes['votes'] = sample_votes
            best_round_votes['err'] = sample_err

        if sample_err < best_sample_votes['err']:
            best_sample_votes['votes'] = sample_votes
            best_sample_votes['err'] = sample_err


        if i % 100000 == 0:
            print('######################################################')
            print('#round:',int(i/round_iterate))
            print('best round vote:', best_round_votes['votes'])
            print('best round mean absolute error:', calculate_mae_error(best_round_votes['votes'], election_result_normalized))
            print('total hits till now:', j)
            print('probability till now:', (100 * (i - j) / i),'%')
            print('######################################################')
            best_round_votes = {'votes': [], 'err':max_error}



    final_result = (100 * (i - j) / i)

    return final_result, best_sample_votes


nlpcontest_mae = calculate_mse_error(nlpcontest_prediction,election_result)
nlpcontest_mae_normalized = calculate_mse_error(nlpcontest_prediction,election_result_normalized)

print(nlpcontest_df)
print('nlpcontest_mae:',nlpcontest_mae )
print('nlpcontest_mae_normalized:',nlpcontest_mae_normalized )
print()

final_result ,  best_sample_votes = calculate_probability_by_monte_carlo_method(nlpcontest_mae,total_iterate )
print()
print('Best sample vote:',best_sample_votes['votes'])
print('Mean absolute error :',best_sample_votes['err'])
print('Final Result :',final_result)