# NLPContest Result

This python snippet calculates the accuracy of NLPContest Results by Monte Carlo Method (https://en.wikipedia.org/wiki/Monte_Carlo_method).

The snippet output on 10 million random generated votes shows that the accuracy of NLPContest Results is always more than 99.99 %.

Feel free to test the code in Google Colab:
https://colab.research.google.com/drive/14CL1XWufmoZij7bcRR3LUgnF1ozcgtwG






